import { Routes as AppRoutes, Route } from "react-router-dom";
import SearchPage from "../components/pages/SearchPage";

const Routes = () => {
  return (
    <AppRoutes>
      <Route path="/search" element={<SearchPage />} />;
    </AppRoutes>
  );
};

export default Routes;
