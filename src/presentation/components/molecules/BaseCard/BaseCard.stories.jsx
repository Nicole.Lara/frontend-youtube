import BaseCard from "./BaseCard";

export default {
  title: "Molecules/BaseCard",
  component: BaseCard,
};

const Template = (args) => <BaseCard {...args} />;

export const PrimaryBaseCard = Template.bind({});
