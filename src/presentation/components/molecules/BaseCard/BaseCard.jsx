import Card from "../../atoms/Card";
import Image from "../../atoms/Image";
import TitleCard from "../../atoms/Typography/TitleCard";
import TextCard from "../../atoms/Typography/TitleCard";
import Box from "../../atoms/Box";
import BoxColumn from "../../atoms/BoxColumn";
import BoxRow from "../../atoms/BoxRow";
import { string } from "prop-types";
//import kitty from "../../../assets/img/kitty.jpg";
import styled from "styled-components";
const BoxStyle = styled(Box)`
  margin-top: 1%;
  margin-left: 2%;
  width: 70%;
`;

const BaseCard = ({key, variantImage, variantName, title, text }) => {
  return (
    <Card>
      <BoxRow>
        <Image src={variantImage} alt="base photo" />
        <BoxColumn>
          <BoxStyle>
            <TitleCard title={title}>{title}</TitleCard>
          </BoxStyle>
          <BoxStyle>
            <TextCard text={text}>{text}</TextCard>
          </BoxStyle>
        </BoxColumn>
      </BoxRow>
    </Card>
  );
};
BaseCard.propTypes = {
  key: string,
  variantImage: string,
  variantName: string,
  title: string,
  text: string,
  
};

export default BaseCard;
