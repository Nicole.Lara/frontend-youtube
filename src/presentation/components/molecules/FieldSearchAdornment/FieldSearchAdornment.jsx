import TextField from "../../atoms/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import { func, string } from "prop-types";
import SearchButton from "../../atoms/SearchButton";
import styled from "styled-components";
const StyleTextField = styled(TextField)`
  .MuiOutlinedInput-root {
    padding-right: 1px;
  }
`;

const FieldSearchAdornment = ({ name, placeholder, onClick, onChange, ...props }) => {
  return (
    <>
      <StyleTextField
        fullWidth
        name={name}
        placeholder={placeholder}
        onChange={onChange}
        {...props}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <SearchButton onClick={onClick}  />
            </InputAdornment>
          ),
        }}
        onKeyDown={(event) => {
          if (event.key === "Enter") {
            onClick();
          }
        }}
      />
    </>
  );
};

FieldSearchAdornment.propTypes = {
  placeholder: string,
  name: string,
  onClick: func,
  onChange: func,
};

export default FieldSearchAdornment;
