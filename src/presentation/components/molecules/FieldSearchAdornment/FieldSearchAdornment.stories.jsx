import FieldSearchAdornment from "./FieldSearchAdornment";

export default {
  title: "Molecules/FieldSearchAdornment",
  component: FieldSearchAdornment,
};

const Template = (args) => <FieldSearchAdornment {...args} />;

export const PrimaryFieldSearchAdornment = Template.bind({});
