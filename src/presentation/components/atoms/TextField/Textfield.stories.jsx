import TextField from "./TextField";

export default {
  title: "Atoms/TextField",
  component: TextField,
};

const Template = (args) => <TextField {...args} />;

export const PrimaryTextField = Template.bind({});
