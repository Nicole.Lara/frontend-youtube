import styled from "styled-components";
import TextFieldMui from "@mui/material/TextField";
import { func, string } from "prop-types";

const Input = styled(TextFieldMui)`
  .MuiOutlinedInput-root {
    height: 36px;
    font-size: 13px;
    margin-top: 1%;
    border-color: #7c7c7c;
    & fieldset {
      border-color: #d1d0d0;
    }
  }

  .Mui-disabled {
    cursor: not-allowed;
  }

  .customtextfield {
    & input::placeholder {
      font-size: 16px;
      line-height: 23px;
    }
  }
`;

const TextField = ({ name, onChange, ...props }) => {
  return (
    <Input
      fullWidth
      variant="outlined"
      type="search"
      InputLabelProps={{ shrink: false }}
      id={name}
      name={name}
      onChange={onChange}
      {...props}
    />
  );
};

TextField.propTypes = {
  placeholder: string,
  name: string,
  onChange: func,
};
TextField.defaultProps = {
  placeholder: "",
};

export default TextField;
