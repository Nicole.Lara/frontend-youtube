import BoxMUI from "@mui/material/Box";
import styled from "styled-components";

const Box = styled(BoxMUI)`
  display: flex;
  width: 100%;
`;

export default Box;
