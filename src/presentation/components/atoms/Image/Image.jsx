import styled from "styled-components";

const Image = styled("img")`
  width: 25%;
  height: auto;
`;

export default Image;
