import Image from "./Image";
import kitty from "../../../assets/img/kitty.jpg";

export default {
  title: "Atoms/Image",
  component: Image,
};

export const Img = () => <Image src={kitty} alt="kitty" />;
