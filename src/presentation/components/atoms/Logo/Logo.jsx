import styled from "styled-components";
import youtube from "../../../assets/img/youtube.svg.webp";

const Img = styled("img")`
  width: 15%;
`;

const Logo = () => {
  return <Img src={youtube} alt="logo youtube" />;
};

export default Logo;
