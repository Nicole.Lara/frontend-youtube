import Logo from "./Logo";

export default {
  title: "Atoms/Logo",
  component: Logo,
};

const Template = (args) => <Logo {...args} />;

export const PrimaryLogo = Template.bind({});
