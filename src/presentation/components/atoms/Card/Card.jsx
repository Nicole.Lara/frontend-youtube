import styled from "styled-components";
import CardMUI from "@mui/material/Card";

const Card = styled(CardMUI)`
  background: #ffffff;
  box-shadow: 0px 0px 3.12996px 1.04332px rgba(0, 0, 0, 0.25);
  border-radius: 8.26805px;
  width: 100%;
  margin: 1% 2% 0% 0%;
`;

export default Card;
