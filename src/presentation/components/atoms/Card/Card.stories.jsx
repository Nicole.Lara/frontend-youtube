import Card from "./Card";

export default {
  title: "Atoms/Card",
  component: Card,
};

const Template = (args) => <Card {...args} />;

export const PrimaryCard = Template.bind({});
