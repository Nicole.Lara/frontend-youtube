import Typography from "@mui/material/Typography";
import styled from "styled-components";

const TitleCard = styled(Typography)`
  font-size: 20px;
  line-height: 20px;
  color: #030303;
  font-weight: bold, 500;
`;
export default TitleCard;
