import styled from "styled-components";
import TitleCard from "./TitleCard";

const TextCard = styled(TitleCard)`
  font-size: 16px;
  line-height: 18px;
  color: #606060;
  font-weight: 400;
`;
export default TextCard;
