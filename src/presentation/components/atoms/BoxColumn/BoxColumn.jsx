import Box from '../../atoms/Box'
import styled from 'styled-components'

const BoxColumn = styled(Box)`
  display: flex;
  width: 100%;
  flex-direction: column;
`

export default BoxColumn
