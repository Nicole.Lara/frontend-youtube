import Box from '../Box'
import styled from 'styled-components'

const BoxRow = styled(Box)`
  display: flex;
  flex-direction: row;
  width: 100%;
`

export default BoxRow
