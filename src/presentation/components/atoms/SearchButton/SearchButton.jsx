import Button from "@mui/material/Button";
import styled from "styled-components";
import SearchIcon from "@mui/icons-material/Search";
import { func } from "prop-types";

const StyleButton = styled(Button)`
  && {
    background-color: #e9e9e9;
    box-shadow: none;
    border-radius: 0px 2px 2px 0px;
    :hover {
      background: #e9e9e9;
      box-shadow: none;
    }
  }
`;
const StyleSearchIcon = styled(SearchIcon)`
  && {
    color: #030303;
    width: 60%;
  }
`;

const SearchButton = ({onClick}, ...props) => {
  return (
    <StyleButton variant="contained" onClick={onClick}  {...props}>
      <StyleSearchIcon />
    </StyleButton>
  );
};
SearchButton.propTypes = {
  onClick: func,
 
};

export default SearchButton;
