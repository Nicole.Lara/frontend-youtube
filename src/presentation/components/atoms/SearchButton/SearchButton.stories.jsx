import SearchButton from "./SearchButton";

export default {
  title: "Atoms/SearchButton",
  component: SearchButton,
};

const Template = (args) => <SearchButton {...args} />;

export const PrimarySearchButton = Template.bind({});
