import HeaderSearch from "./HeaderSearch";

export default {
  title: "Organisms/HeaderSearch",
  component: HeaderSearch,
};

const Template = (args) => <HeaderSearch {...args} />;

export const PrimaryHeaderSearch = Template.bind({});
