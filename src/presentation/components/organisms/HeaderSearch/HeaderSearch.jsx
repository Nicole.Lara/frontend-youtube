import Box from "../../atoms/Box";
import Logo from "../../atoms/Logo";
import FieldSearchAdornment from "../../molecules/FieldSearchAdornment";
import styled from "styled-components";
import { func, string } from "prop-types";


const BoxField = styled(Box)`
  width: 80%;
  margin-left: 5%;
`;

const HeaderSearch = ({ name, onClick, onChange}) => {


  return (
    <Box>
      <Logo />
      <BoxField>
        <FieldSearchAdornment 
              name={name}
              placeholder="placeholder"
              onChange={onChange}
              onClick={onClick}/>
      </BoxField>
    </Box>
  );
};

HeaderSearch.propTypes = {
  name: string,
  onClick: func,
  onChange: func,
};

export default HeaderSearch;
