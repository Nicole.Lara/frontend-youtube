import BaseCard from "../../molecules/BaseCard";
import Box from "../../atoms/Box";
import BoxColumn from "../../atoms/BoxColumn";
import kitty from "../../../assets/img/kitty.jpg";
import styled from "styled-components";

const BoxStyle = styled(Box)`
  flex-wrap: wrap;
  margin: 5%;
`;

const VariantBaseCard = ({data}) => {
  return (
    <BoxColumn>
      <BoxStyle>
        {data.map((variant) => (
          <BaseCard
            key={variant.variantId}
            variantImage={variant.variantImage}
            variantName={variant.variantName}
            title={variant.title}
            text={variant.text}
          />
        ))}
      </BoxStyle>
    </BoxColumn>
  );
};

export default VariantBaseCard;
