import VariantBaseCard from "./VariantBaseCard";

export default {
  title: "Organisms/VariantBaseCard",
  component: VariantBaseCard,
};

const Template = (args) => <VariantBaseCard {...args} />;

export const PrimaryVariantBaseCard = Template.bind({});
