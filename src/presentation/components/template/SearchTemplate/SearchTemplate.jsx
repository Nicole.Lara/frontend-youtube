import BoxColumn from "../../atoms/BoxColumn";
import Box from "../../atoms/Box";
import HeaderSearch from "../../organisms/HeaderSearch";
import VariantBaseCard from "../../organisms/VariantBaseCard/VariantBaseCard";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useState, useCallback, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import useQueryParams from '../../../hooks/useQueryParams'

const BoxColumnStyle = styled(BoxColumn)`
  width: 90%;
  margin-top: 5%;
  margin-left: 2%;
`;
const BoxStyle = styled(Box)`
  margin-left: 4%;
`;
const SearchTemplate = () => {

  const [submitting, setSubmitting] = useState(true)
  const [data, setData] = useState([])
  const [query, setQuery] = useState("")
  const [name, setName] = useState("button-gato")






  const getData = useCallback(async () => {
    console.log(query);
    if(query){
      const response = await fetch(`https://youtube.googleapis.com/youtube/v3/search?channelType=any&key=AIzaSyCRfP1vl1vxrPKgi7SVwMdgRpRhtjgbVnY&part=snippet&q=${query}`);
      const data1 = await response.json();
      const videos = [];
      data1.items.map(({ id, snippet }) => {
        const elemento = {
          variantId: id.videoId,
          variantImage: snippet.thumbnails.default.url,
          title: snippet.title,
          text: snippet.description,
        };
        console.log(snippet.thumbnails.default.url);
        videos.push(elemento);
        setData(videos);
      });  

      console.log(data1);
    }

    
    
  }, [query])


  useEffect(() => {
    if (submitting) { // is true initially, and again when button is clicked
      getData().then(() => setSubmitting(false))
    }
  }, [submitting, getData])



  const handleChange = event => {
    
    event.preventDefault()
    setQuery(event.target.value)
    
  }

  return (
    <BoxColumnStyle>
      <BoxStyle>
        <HeaderSearch
              name = {name}
              onChange={handleChange}
              onClick={() => setSubmitting(true)}
              />
      </BoxStyle>
      <VariantBaseCard data={data} />
    </BoxColumnStyle>
  );
};

export default SearchTemplate;
