import SearchTemplate from "./SearchTemplate";

export default {
  title: "Template/SearchTemplate",
  component: SearchTemplate,
};

const Template = (args) => <SearchTemplate {...args} />;

export const PrimarySearchTemplate = Template.bind({});
