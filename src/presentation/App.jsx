import { BrowserRouter } from "react-router-dom";
import Routes from "./Routes/Routes";
import { Provider } from 'react-redux'
import { store } from '../domain/store/store'

function App() {
  return (
   
    <BrowserRouter basename={"/"}>
      <Routes />
    </BrowserRouter>

  );
}

export default App;
